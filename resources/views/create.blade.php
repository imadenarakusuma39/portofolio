@extends('layouts.default')

@section('content')
    <section>
        <div class="container mt-5">
            <h1>Tambah Mahasiswa</h1>
            <div class="row">
                <div class="col-lg-B">
                    <form action="{{ url('/store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="nama">Nama Mahasiswa</label>
                            <input type="text" name="nama_mahasiswa" class="form-control" placeholder="Masukkan Nama Anda">
                        </div>
                        <div class="form-group">
                            <label for="nim">NIM Mahasiswa</label>
                            <input type="number" name="nim_mahasiswa" class="form-control" placeholder="Masukkan NIM Anda">
                        </div>
                        <div class="form-group">
                            <label for="kelas">Kelas Mahasiswa</label>
                            <input type="text" name="kelas_mahasiswa" class="form-control" placeholder="Masukkan Kelas Anda">
                        </div>
                        <div class="form-group">
                            <label for="prodi">Prodi Mahasiswa</label>
                            <input type="text" name="prodi_mahasiswa" class="form-control" placeholder="Masukkan Prodi Anda">
                        </div>
                        <div class="form-group">
                            <label for="fakultas">Fakultas Mahasiswa</label>
                            <input type="text" name="fakultas_mahasiswa" class="form-control" placeholder="Masukkan Fakultas Anda">
                        </div>
                        <div class="form-group mt-2">
                            <button type="submit" class="btn btn-success">Tambah Mahasiswa</button>
                        </div>
                        <div class="form-group mt-2">
                            <a href="{{ url('/') }}">Kembali ke halaman utama</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection