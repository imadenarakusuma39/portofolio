@extends('layouts.default')

@section('content')
    <section>
        <div class="container mt-5">
            <h1>Edit Mahasiswa</h1>
            <div class="row">
                <div class="col-lg-B">
                    <form action="{{ url('/update/'.$data->id) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="nama">Nama Mahasiswa</label>
                            <input type="text" name="nama_mahasiswa" class="form-control" placeholder="Masukkan Nama Anda" value="{{ $data->nama_mahasiswa }}">
                        </div>
                        <div class="form-group">
                            <label for="nim">NIM Mahasiswa</label>
                            <input type="number" name="nim_mahasiswa" class="form-control" placeholder="Masukkan NIM Anda" value="{{ $data->nim_mahasiswa }}">
                        </div>
                        <div class="form-group">
                            <label for="kelas">Kelas Mahasiswa</label>
                            <input type="text" name="kelas_mahasiswa" class="form-control" placeholder="Masukkan Kelas Anda" value="{{ $data->kelas_mahasiswa }}">
                        </div>
                        <div class="form-group">
                            <label for="prodi">Prodi Mahasiswa</label>
                            <input type="text" name="prodi_mahasiswa" class="form-control" placeholder="Masukkan Prodi Anda" value="{{ $data->prodi_mahasiswa }}">
                        </div>
                        <div class="form-group">
                            <label for="fakultas">Fakultas Mahasiswa</label>
                            <input type="text" name="fakultas_mahasiswa" class="form-control" placeholder="Masukkan Fakultas Anda" value="{{ $data->fakultas_mahasiswa }}">
                        </div>
                        <div class="form-group mt-2">
                            <button type="submit" class="btn btn-success">Simpan Perubahan</button>
                        </div>
                        <div class="form-group mt-2">
                            <a href="{{ url('/') }}">Kembali ke halaman utama</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection